/*
 * Author: Tate Kennington - 5925152
*/

package main

import "os";
import "fmt";
import "bufio";

//Output the two byte slices as formated text
func printText(a, b []byte){
	var aOut = make([]byte, len(a));
	var bOut = make([]byte, len(b));

	//Consider every byte
	for i:=0; i<len(a); i++{
		//Copy the byte to the output slice
		aOut[i] = a[i];
		bOut[i] = b[i];

		//If the byte is a placeholder replace it
		if aOut[i] == '^'{
			aOut[i] = ' ';
		}
		if bOut[i] == '^'{
			bOut[i] = ' ';
		}
	}

	//Output the slices
	fmt.Printf("%s\n\n%s\n", string(aOut), string(bOut));
}

//Determine if a byte slice looks like valid text
func isValid(buffer []byte) bool{

	//Consider every byte
	for i:=0; i<len(buffer); i++{

		//The byte is a lowercase letter
		if buffer[i]>='a' && buffer[i]<='z'{
			continue;
		}

		//The byte is an uppercase letter
		if buffer[i]>='A' && buffer[i]<='Z'{
			if i-1>=0 && buffer[i-1] != ' '{
				return false;
			}
			continue;
		}

		//The byte is a numeral
		/*if buffer[i] >= '0' && buffer[i] <= '9'{
			continue;
		}*/

		//The byte is a punctuation character
		if buffer[i] == ' ' || buffer[i] == '.' || buffer[i] == ',' /*|| buffer[i] == '?' || buffer[i] == '!'*/{
			continue;
		}
		
		return false;
	}
	return true;
}

//Main entry point
func main(){
	var cipherFile, _ = os.Open("cipher");
	var cipher = make([]byte, 0);
	var data = make([]byte, 1);
	var kb = bufio.NewScanner(os.Stdin);

	//Read in the encrypted text
	n, _ := cipherFile.Read(data);
	for n!=0{
		cipher = append(cipher, data[0]);
		n, _ = cipherFile.Read(data);	
	}

	//Inititalize the plaintext slices
	var output = [2][]byte{make([]byte, len(cipher)), make([]byte, len(cipher))};
	for i:=0; i<len(output[0]); i++{
		output[0][i] = '^';
		output[1][i] = '^';
	}

	//Prompt the user if they want to load a partial decryption
	fmt.Println("Load?");
	kb.Scan();
	if kb.Text() == "y"{

		//Load partial decryption
		var loadFile, _ = os.Open("save");
		n, _ = loadFile.Read(data);
		total := 0;
		for n!=0{
			output[0][total] = data[0];
			if output[0][total] != '^'{
				output[1][total] = cipher[total] ^ output[0][total];
			}
			total+=n;
			n, _ = loadFile.Read(data);
		}
		loadFile.Close();
		fmt.Println("Loaded:");
		printText(output[0], output[1]);
	}

	var invalid = make(map[string]bool);
	
	for true{
		//Prompt the user for a word to drag
		fmt.Println("Enter word:");
		kb.Scan();
		var word = kb.Text();

		//Quit if the word is q
		if word == "q"{
			break;
		}
		
		var buffer = make([]byte, len(word));

		//For every position in the ciphertext
		for i:=0; i<len(cipher)-len(buffer); i++{

			//xor the search word at that position
			copy(buffer, cipher[i:i+len(buffer)]);
			for j, _ := range word{
				buffer[j] ^= word[j];
			}

			//If the result is valid
			if isValid(buffer) && !invalid[string(buffer)]{

				//Prompt the user for confirmation
				fmt.Println(string(buffer));
				kb.Scan();

				//If they confirm
				if kb.Text() == "y"{

					//Display the change in output to the user
					var oldOutput = [2]string{string(output[0]),string(output[1])};
					fmt.Println("Current:");
					printText(output[0], output[1]);
					copy(output[0][i:i+len(buffer)], buffer);
					copy(output[1][i:i+len(word)], []byte(word));
					fmt.Println("New:");
					printText(output[0], output[1]);

					//Prompt the user for action
					kb.Scan();

					//Revert the plaintext back
					if kb.Text() == "n"{
						output[0] = []byte(oldOutput[0]);
						output[1] = []byte(oldOutput[1]);
					}

					//Swap the plaintext the word occurs in
					if kb.Text() == "s"{
						copy(output[1][i:i+len(buffer)], buffer);
						copy(output[0][i:i+len(word)], []byte(word));
						fmt.Println("New:");
						printText(output[0], output[1]);
					}
				} else {
					//Remember this string as invalid
					invalid[string(buffer)] = true;
				}
			}
		}
	}

	//Prompt the user to save the partial decryption
	printText(output[0], output[1]);
	fmt.Println("Save?");
	kb.Scan();
	if kb.Text() == "y"{
		//Save the partial decryption
		var saveFile, _ = os.Create("save");
		saveFile.Write(output[0]);
		saveFile.Close();
	}
}
