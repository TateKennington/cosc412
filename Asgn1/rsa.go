package main

import "fmt"
import "math/big"

func main(){
	var pString, qString, cipherString string;
	fmt.Scanf("%s %s %s", &pString, &qString, &cipherString);
	var p = big.NewInt(0);
	var q = big.NewInt(0);
	var cipher = big.NewInt(0);
	for i:=0; i<len(pString) || i<len(qString) || i<len(cipherString); i++{
		if i<len(pString){
			p.Mul(p, big.NewInt(10));
			p.Add(p,big.NewInt(int64(pString[i]-'0')));
		}
		if i<len(qString){
			q.Mul(q, big.NewInt(10));
			q.Add(q,big.NewInt(int64(qString[i]-'0')));
		}
		if i<len(cipherString){
			cipher.Mul(cipher, big.NewInt(10));
			cipher.Add(cipher,big.NewInt(int64(cipherString[i]-'0')));
		}
	}
	var n = big.NewInt(0)
	n.Mul(big.NewInt(0).Add(p, big.NewInt(-1)), big.NewInt(0).Add(q,big.NewInt(-1)));
	var e = big.NewInt(234571);
	var d = big.NewInt(1).ModInverse(e, n);
	fmt.Print("N: ");
	fmt.Println(n);
	fmt.Print("pq: ");
	fmt.Println(big.NewInt(0).Mul(p,q));
	fmt.Print("d: ");
	fmt.Println(d);
	fmt.Print("ed: ");
	var ed = big.NewInt(0).Mul(e,d);
	fmt.Println(ed);
	fmt.Println(ed.Mod(ed,n));
	fmt.Print("m: ")
	fmt.Println(cipher.Exp(cipher, d, big.NewInt(0).Mul(p,q)));
	fmt.Print("10: ");
	var ten = big.NewInt(10);
	fmt.Println(ten.Exp(ten, e,big.NewInt(0).Mul(p,q)).Exp(ten, d, big.NewInt(0).Mul(p,q)));
}
