package main

import "os";
import "fmt";

func main(){
	var cpFile, _ = os.Open("cp");
	var cqFile, _ = os.Open("cq");
	var output = make([]byte,0);
	
	var cpData = make([]byte,1);
	var cqData = make([]byte,1);
	n, _ :=cpFile.Read(cpData);
	m, _ :=cqFile.Read(cqData);
	for n != 0 && m != 0 {
		output = append(output,cpData[0]^cqData[0]);
		n, _ =cpFile.Read(cpData);
		m, _ =cqFile.Read(cqData);
	}
	fmt.Println(output);
	var outputFile, _ = os.Create("cipher");
	for n, _ =outputFile.Write(output); n!=len(output); n, _ = outputFile.Write(output){
		output = output[n:];
	}
	outputFile.Close();
	cpFile.Close();
	cqFile.Close();
}
